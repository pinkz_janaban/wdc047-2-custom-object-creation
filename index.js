// Quiz

/*
#1 What is the term given to unorganized code that's very hard to work with?
Ans: Spaghetti Code

#2 How are object literals written in JS?
Ans: {} 
}

#3 What do you call the concept of organizing information and functionality to belong to an object?
Ans:
encapsulation

#4 

#5 True or False: Objects can have objects as properties. 
Ans: True

#6 What is the syntax in creating key-value pairs?
Ans: 
Syntax:
let/const objectName = {
	keyA: valueA,
	keyB: valueB
}

#7 True or False: A method can have no parameters and still work.
Ans: False

#8 True or False: Arrays can have objects as elements
Ans: True

#9 True or False: Arrays are objects.
Ans: False

#10 True or False: Objects can have arrays as properties.
Ans: True

*/

// Functon Coding
// #1-4

let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],

	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum/4;
	},

	willPass(){
		return this.computeAve() >=85 ? true : false
	},

	willPassWithHonors(){
		if(this.willPass() && this.computeAve() >= 90) {
			return true
		} else if (this.willPass() && this.computeAve()  < 90){
			return false
		}
		else if (this.willPass() && this.computeAve() < 85){
			return undefined
		}
	},
}



let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [78, 82, 79, 85],

	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum/4;
	},

	willPass(){
		return this.computeAve() >=85 ? true : false
	},
	willPassWithHonors(){
		if(this.willPass() && this.computeAve() >= 90) {
			return true
		} else if (this.willPass() && this.computeAve()  < 90){
			return false
		}
		else if (this.willPass() && this.computeAve() < 85){
			return undefined
		}
	},

}

let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],

	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum/4;
	},
	willPass(){
		return this.computeAve() >=85 ? true : false
	},
	willPassWithHonors(){
		if(this.willPass() && this.computeAve() >= 90) {
			return true
		} else if (this.willPass() && this.computeAve()  < 90){
			return false
		}
		else if (this.willPass() && this.computeAve() < 85){
			return undefined
		}
	},

}

let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],

	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum/4;
	},
	willPass(){
		return this.computeAve() >=85 ? true : false
	},
	willPassWithHonors(){
		if(this.willPass() && this.computeAve() >= 90) {
			return true
		} else if (this.willPass() && this.computeAve()  < 90){
			return false
		}
		else if (this.willPass() && this.computeAve() < 85){
			return undefined
		}
	},
}


// #5-6

let classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],

	countHonorStudents(){
		
		count = this.students.willPassWithHonors.reduce((num, classOf1A) => num + classOf1A, 0);
		return count;
	},
};


	
